#pragma once

#include <cstdint>
#include <arpa/inet.h>
#include <memory>
#include <unistd.h>

namespace {

template <typename T>
T from_be(T t);

template <typename T>
T to_be(T t);

template <>
int32_t from_be(int32_t i)
{
  return ntohl(i);
}

template <>
uint32_t from_be(uint32_t u)
{
  return ntohl(u);
}

template <>
int32_t to_be(int32_t i)
{
  return htonl(i);
}

template <>
uint32_t to_be(uint32_t u)
{
  return htonl(u);
}


template <>
int16_t from_be(int16_t i)
{
  return ntohs(i);
}

template <>
uint16_t from_be(uint16_t u)
{
  return ntohs(u);
}

template <>
int16_t to_be(int16_t i)
{
  return htons(i);
}

template <>
uint16_t to_be(uint16_t u)
{
  return htons(u);
}


template <>
int8_t from_be(int8_t i)
{
  return i;
}

template <>
uint8_t from_be(uint8_t u)
{
  return u;
}

template <>
int8_t to_be(int8_t i)
{
  return i;
}

template <>
uint8_t to_be(uint8_t u)
{
  return u;
}


template <typename Skt, typename Fn, typename I>
I rw_exact(Skt skt, char *buf, I len, const Fn &fn)
{
  I bytes_done = 0;
  I ret;
  while (bytes_done < len) {
    ret = fn(skt, buf +bytes_done, len -bytes_done);
    if (ret <= 0) {
      throw int(ret);
    }
    bytes_done += ret;
  }
  return bytes_done;
}

} //ns

template <typename T>
T read_packet(int from, std::unique_ptr <char []> &msg)
{
  T len;
  rw_exact(from, (char *)&len, sizeof(T),
    [] (int s, char *b, int l) -> int {return ::read(s, b, l);});
  len = from_be(len);
  msg.reset(new char [len]);
  rw_exact(from, &(msg[0]), len, ::read);
  return len;
}

template <typename T>
void write_packet(int to, char *buf, T len)
{
  T l = to_be(len);
  auto f = [] (int s, char *b, int l) -> int {return ::write(s, b, l);};
  rw_exact(to, (char *)&l, sizeof(T), f);
  rw_exact(to, buf, len, f);
}

