#pragma once

enum tlve_type {
  TLVE_TUPLE,
  TLVE_LIST,
  TLVE_BINARY,
  TLVE_ATOM,
  TLVE_INTEGER,
  TLVE_ERROR = 9000
};


#ifdef __cplusplus
extern "C" {
#endif

enum tlve_type tlve_get_next_type(const char *buf, int index);
int tlve_check_magic(const char *buf, int *index);
int tlve_get_tuple(const char *buf, int *index);
int tlve_get_list(const char *buf, int *index);
int tlve_get_binary(const char *buf, int *index, const char **bin);
int tlve_get_atom(const char *buf, int *index, const char **atom);
int tlve_get_integer(const char *buf, int *index, const char **integer);

void tlve_set_magic(char *buf, int *index);
void tlve_set_tuple(char *buf, int len, int *index);
void tlve_set_list(char *buf, int len, int *index);
void tlve_set_binary(char *buf, int len, char **binary, int *index);
void tlve_set_atom(char *buf, int len, char **atom, int *index);
void tlve_set_integer(char *buf, int len, char **integer, int *index);


#ifdef __cplusplus
}
#endif
