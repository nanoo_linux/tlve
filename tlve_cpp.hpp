#pragma once

#include <memory>
#include "tlve.h"


class tlve_encoder
{
  std::unique_ptr <char []> buffer_ref_;
  char *buffer_;
  int index_;

  public:
    tlve_encoder(size_t);
    tlve_encoder(char *);
    char *get_data();
    int get_data_len();
    tlve_encoder &tuple(int);
    tlve_encoder &list(int);
    tlve_encoder &binary(int, char **);
    tlve_encoder &atom(int, char **);
    tlve_encoder &atom(const char *);
    tlve_encoder &integer(int, char **);
    tlve_encoder &integer(int);
    tlve_encoder &prepare_rpc_call(const char *, const char *);
    tlve_encoder &prepare_rpc_call(const char *);
    tlve_encoder &prepare_rpc_reply();
    tlve_encoder &prepare_rpc_error();
    tlve_encoder &prepare_rpc_event(const char *, const char *);
    tlve_encoder &prepare_rpc_event(const char *);
};

//////////////////
// in case of decoding error all funs throw -1;

class tlve_decoder
{
  const char *buffer_;
  int index_;

  bool is_rpc(const char *, const char *, char);
  bool is_rpc(const char *, char);
  bool is_rpc_back(char);

  public:
    enum type {TLVE_TUPLE,
      TLVE_LIST,
      TLVE_BINARY,
      TLVE_ATOM,
      TLVE_INTEGER,
      TLVE_ERROR};

    tlve_decoder(const char *);

    enum type next_type();

    int tuple();
    int list();
    int binary(const char **);
    int atom(const char **);
    int integer(const char **);
    int integer();
    bool is_rpc_call(const char *, const char *);
    bool is_rpc_call(const char *);
    bool is_rpc_event(const char *, const char *);
    bool is_rpc_event(const char *);
    bool is_rpc_reply();
    bool is_rpc_error();
};
