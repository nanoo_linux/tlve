#include "host_to_net.hpp"

#include <cstdio>


template <>
void host_to_net <2, false> (char *from, char *to)
{
  to[0] = from[1];
  to[1] = from[0];
}

template <>
void host_to_net <3, false> (char *from, char *to)
{
  to[0] = from[2];
  to[1] = from[1];
  to[2] = from[0];
}

template <>
void host_to_net <4, false> (char *from, char *to)
{
  to[0] = from[3];
  to[1] = from[2];
  to[2] = from[1];
  to[3] = from[0];
}

template <>
void host_to_net <2, true> (char *from, char *to)
{
  to[0] = from[0];
  to[1] = from[1];
}

template <>
void host_to_net <3, true> (char *from, char *to)
{
  to[0] = from[0];
  to[1] = from[1];
  to[2] = from[2];
}

template <>
void host_to_net <4, true> (char *from, char *to)
{
  to[0] = from[0];
  to[1] = from[1];
  to[2] = from[2];
  to[3] = from[3];
}

