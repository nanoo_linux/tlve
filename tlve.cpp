#include "tlve.hpp"

#include "tlve.h"

namespace {

  class iDecodeResultAcceptor {
    public:
      virtual ~iDecodeResultAcceptor() {}
      virtual void set(std::shared_ptr <tlve::Element> &) = 0;
  };

  class SimpleDecodeResultAcceptor : public iDecodeResultAcceptor {
    std::shared_ptr <tlve::Element> result_;
    public:
      void set(std::shared_ptr <tlve::Element> &e) {
        result_ = e;
      }
      std::shared_ptr <tlve::Element> result() const {
        return result_;
      }
  };

  class CompoundDecodeResultAcceptor : public iDecodeResultAcceptor {
    std::shared_ptr <tlve::Compound> result_;
    public:
      CompoundDecodeResultAcceptor(std::shared_ptr <tlve::Compound> &r)
        :result_(r) {}
      void set(std::shared_ptr <tlve::Element> &e) {
        result_->add(e);
      }
  };

  void decode(const char *data, int &index, iDecodeResultAcceptor &target)
  {
    std::shared_ptr <tlve::Element> decoded;
    const char *tmp_ptr;
    int tmp_len;

    switch (tlve_get_next_type(data, index)) {
      case TLVE_TUPLE:
        tmp_len = tlve_get_tuple(data, &index);
        if (tmp_len < 0) {throw tlve::TupleError();}
        {
          std::shared_ptr <tlve::Compound> d1 =
            std::make_shared <tlve::Tuple> ();
          CompoundDecodeResultAcceptor acceptor(d1);
          for (int i=0; i<tmp_len; ++i) {
            decode(data, index, acceptor);
          }
          decoded = d1;
        }
        break;
      case TLVE_LIST:
        tmp_len = tlve_get_list(data, &index);
        if (tmp_len < 0) {throw tlve::ListError();}
        {
          std::shared_ptr <tlve::Compound> d1 =
            std::make_shared <tlve::List> ();
          CompoundDecodeResultAcceptor acceptor(d1);
          for (int i=0; i<tmp_len; ++i) {
            decode(data, index, acceptor);
          }
          decoded = d1;
        }
        break;
      case TLVE_BINARY:
        tmp_len = tlve_get_binary(data, &index, &tmp_ptr);
        if (tmp_len < 0) {throw tlve::BinaryError();}
        decoded = std::make_shared <tlve::Binary> (tmp_ptr, tmp_len);
        break;
      case TLVE_ATOM:
        tmp_len = tlve_get_atom(data, &index, &tmp_ptr);
        if (tmp_len < 0) {throw tlve::AtomError();}
        decoded = std::make_shared <tlve::Atom> (tmp_ptr, tmp_len);
        break;
      case TLVE_INTEGER:
        tmp_len = tlve_get_integer(data, &index, &tmp_ptr);
        if (tmp_len < 0) {throw tlve::IntegerError();}
        decoded = std::make_shared <tlve::Integer> (tmp_ptr, tmp_len);
        break;
      case TLVE_ERROR:
        throw tlve::TypeError();
    }
    target.set(decoded);
  }

}


namespace tlve {

  void Compound::add(const std::shared_ptr <Element> &e) {
    elements_.push_back(e);
  }

  size_t Compound::length() const {
    return elements_.size();
  }

  std::shared_ptr <Element> &
  Compound::operator [] (size_t index) {
    return elements_[index];
  }

  Terminal::Terminal(const char *ptr, size_t len)
  :ptr_(ptr), len_(len) {}

  const char *Terminal::data() const {return ptr_;}

  size_t Terminal::length() const {return len_;}

  void Tuple::accept(ElementAcceptor &a) {a.accept_element(*this);}

  void List::accept(ElementAcceptor &a) {a.accept_element(*this);}

  Binary::Binary(const char *d, size_t l) : Terminal(d, l) {}
  void Binary::accept(ElementAcceptor &a) {a.accept_element(*this);}

  Atom::Atom(const char *d, size_t l) : Terminal(d, l) {}
  void Atom::accept(ElementAcceptor &a) {a.accept_element(*this);}

  std::string Atom::value() const {return std::string(data(), length());}

  Integer::Integer(const char *d, size_t l) : Terminal(d, l) {}
  void Integer::accept(ElementAcceptor &a) {a.accept_element(*this);}

  Integer::operator int() {
    int ret;
    size_t l = length();
    const unsigned char *dd =
      reinterpret_cast <const unsigned char *> (data());
    switch (l) {
      case 1:
        ret = *dd; ret <<= 24; ret >>= 24;
        break;
      case 2:
        ret = dd[0] * 256 + dd[1]; ret <<= 16; ret >>= 16;
        break;
      case 3:
        ret = dd[0] * 65536 + dd[1] * 256 + dd[2]; ret <<= 8; ret >>= 8;
        break;
      case 4:
        ret = dd[0] * 16777216 + dd[1] * 65536 + dd[2] * 256 + dd[3];
        break;
      default:
        throw IntegerNotImplemented();
    }
    return ret;
  }

  std::shared_ptr <Element> decode(const char *data, size_t len) {
    int index = 0;
    if (tlve_check_magic(data, &index) != 0) {
      throw MagicError();
    }

    SimpleDecodeResultAcceptor acceptor;
    decode(data, index, acceptor);

    return acceptor.result();
  }

} // tlve
