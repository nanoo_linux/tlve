#pragma once

#include <vector>
#include <memory>
#include <string>
#include <exception>


namespace tlve {

  class ElementAcceptor;

  class Element {
    public:
      virtual ~Element() {}
      virtual void accept(ElementAcceptor &) = 0;
  };

  class Compound : public Element
  {
    std::vector <std::shared_ptr <Element> > elements_;
    public:
      void add(const std::shared_ptr <Element> &);
      size_t length() const;
      std::shared_ptr <Element> &operator [] (size_t);
  };

  class Terminal : public Element
  {
    const char *ptr_;
    size_t len_;
    public:
      Terminal(const char *ptr, size_t len);
      const char *data() const;
      size_t length() const;
  };

  class Tuple : public Compound
  {
    public:
      void accept(ElementAcceptor &);
  };

  class List : public Compound
  {
    public:
      void accept(ElementAcceptor &);
  };

  class Binary : public Terminal
  {
    public:
      Binary(const char *, size_t);
      void accept(ElementAcceptor &);
  };

  class Atom : public Terminal
  {
    public:
      Atom(const char *, size_t);
      std::string value() const;
      void accept(ElementAcceptor &);
  };

  class Integer : public Terminal
  {
    public:
      Integer(const char *, size_t);
      void accept(ElementAcceptor &);
      operator int ();
  };


  class ElementAcceptor {
    public:
      virtual ~ElementAcceptor() {}
      virtual void accept_element(Tuple &) = 0;
      virtual void accept_element(List &) = 0;
      virtual void accept_element(Binary &) = 0;
      virtual void accept_element(Atom &) = 0;
      virtual void accept_element(Integer &) = 0;
  };

  class DecodingError : public std::exception
  {
    std::string what_;
    int code_;
    public:
      DecodingError(const char *what, int code): what_(what), code_(code) {}
      ~DecodingError() throw() {}
      const char *what() const throw() {return what_.c_str();}
      int code() const throw() {return code_;}
  };

  class MagicError : public DecodingError {
    public:
      MagicError() : DecodingError("Wrong magic", -1) {}
  };

  class BinaryError : public DecodingError {
    public:
      BinaryError() : DecodingError("Wrong binary", -2) {}
  };

  class AtomError : public DecodingError {
    public:
      AtomError() : DecodingError("Wrong atom", -3) {}
  };

  class IntegerError : public DecodingError {
    public:
      IntegerError() : DecodingError("Wrong atom", -4) {}
  };

  class TypeError : public DecodingError {
    public:
      TypeError() : DecodingError("Wrong atom", -5) {}
  };

  class ListError : public DecodingError {
    public:
      ListError() : DecodingError("Wrong list", -6) {}
  };

  class TupleError : public DecodingError {
    public:
      TupleError() : DecodingError("Wrong atom", -7) {}
  };

  class IntegerNotImplemented : public DecodingError {
    public:
      IntegerNotImplemented() : DecodingError("Integer is too long", -8) {}
  };

  std::shared_ptr <Element> decode(const char *, size_t);

}
