#pragma once

template <typename T>
class Recoil
{
  bool engaged_;
  T f_;

  public:
    Recoil(const T &f) : engaged_(true), f_(f) {}
    void discharge() {engaged_ = false;}
    ~Recoil() {if (engaged_) f_();}
};
