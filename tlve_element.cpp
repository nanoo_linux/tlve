#include "tlve_element.hpp"
#include "tlve_cpp.hpp"


namespace {

p_tlve_element make_tuple(tlve_decoder &);
p_tlve_element make_list(tlve_decoder &);
p_tlve_element make_atom(tlve_decoder &);
p_tlve_element make_binary(tlve_decoder &);
p_tlve_element make_integer(tlve_decoder &);

p_tlve_element make_tuple(tlve_decoder &decoder)
{
  const int sz = decoder.tuple();
  p_tlve_element ret(new tlve_tuple());
  ret->size() = sz;
  for (int i=0; i<sz; ++i) {
    switch (decoder.next_type()) {
      case tlve_decoder::TLVE_TUPLE:
        ret->elements().push_back(make_tuple(decoder));
        break;
      case tlve_decoder::TLVE_LIST:
        ret->elements().push_back(make_list(decoder));
        break;
      case tlve_decoder::TLVE_ATOM:
        ret->elements().push_back(make_atom(decoder));
        break;
      case tlve_decoder::TLVE_BINARY:
        ret->elements().push_back(make_binary(decoder));
        break;
      case tlve_decoder::TLVE_INTEGER:
        ret->elements().push_back(make_integer(decoder));
        break;
      case tlve_decoder::TLVE_ERROR:
        printf("%d\n", __LINE__);
        throw -1;
    }
  }
  return ret;
}

p_tlve_element make_list(tlve_decoder &decoder)
{
  const int sz = decoder.list();
  p_tlve_element ret(new tlve_list());
  ret->size() = sz;
  for (int i=0; i<sz; ++i) {
    switch (decoder.next_type()) {
      case tlve_decoder::TLVE_TUPLE:
        ret->elements().push_back(make_tuple(decoder));
        break;
      case tlve_decoder::TLVE_LIST:
        ret->elements().push_back(make_list(decoder));
        break;
      case tlve_decoder::TLVE_ATOM:
        ret->elements().push_back(make_atom(decoder));
        break;
      case tlve_decoder::TLVE_BINARY:
        ret->elements().push_back(make_binary(decoder));
        break;
      case tlve_decoder::TLVE_INTEGER:
        ret->elements().push_back(make_integer(decoder));
        break;
      case tlve_decoder::TLVE_ERROR:
      printf("%d\n", __LINE__);
        throw -1;
    }
  }
  return ret;
}

p_tlve_element make_atom(tlve_decoder &decoder)
{
  const char *atom_bin;
  int sz = decoder.atom(&atom_bin);
  std::string atom(atom_bin, sz);
  return p_tlve_element(new tlve_atom(atom));
}

p_tlve_element make_binary(tlve_decoder &decoder)
{
  const char *bin;
  int sz = decoder.binary(&bin);
  return p_tlve_element(new tlve_binary(bin, sz));
}

p_tlve_element make_integer(tlve_decoder &decoder)
{
  int integer = decoder.integer();
  return p_tlve_element(new tlve_integer(integer));
}

}//


p_tlve_element tlve_to_element(const char *d)
{
  tlve_decoder decoder(d);
  switch (decoder.next_type()) {
    case tlve_decoder::TLVE_TUPLE:
      return make_tuple(decoder);
    case tlve_decoder::TLVE_LIST:
      return make_list(decoder);
    case tlve_decoder::TLVE_ATOM:
      return make_atom(decoder);
    case tlve_decoder::TLVE_INTEGER:
      return make_integer(decoder);
    case tlve_decoder::TLVE_BINARY:
      return make_binary(decoder);
    case tlve_decoder::TLVE_ERROR:
    printf("%d\n", __LINE__);
      throw -1;
  }
  return p_tlve_element();
}

