#include "tlve.h"

//////////////
// decoding helpers

static int decode_len(const char *uf, int *start_offset)
{
  const unsigned char *buf = (const unsigned char *)uf;
  char type = buf[0] & 0x10;
  char len = buf[0] & 0xf;
  if (type == 0) {
    *start_offset = 1;
    return len;
  } else {
    *start_offset = 1 + len;
    switch (len) {
      case 1:
        return buf[1];
      case 2:
        return buf[1] * 256 + buf[2];
      case 3:
        return buf[1] * 65536 + buf[2] * 256 + buf[3];
      case 4:
        return buf[1] * 16777216 + buf[2] * 65536 + buf[3] * 256 + buf[4];
      default:
        return -1;
    }
  }
}

static int get_base_element(const char *buf, int *index, const char **element)
{
  int start_offset;
  int len = decode_len(buf+*index, &start_offset);
  if (len < 0) {
    return len;
  } else {
    *element = buf + *index + start_offset;
    *index += start_offset + len;
    return len;
  }
}

static int get_recu_element(const char *buf, int *index)
{
  int start_offset;
  int len = decode_len(buf+*index, &start_offset);
  if (len < 0) {
    return len;
  } else {
    *index += start_offset;
    return len;
  }
}


/////////////////
// decoding
enum tlve_type tlve_get_next_type(const char *buf, int index)
{
  int code = (buf[index] & 0xe0) >> 5;
  switch (code) {
    case 0: return TLVE_TUPLE;
    case 1: return TLVE_LIST;
    case 2: return TLVE_BINARY;
    case 3: return TLVE_ATOM;
    case 4: return TLVE_INTEGER;
    default: return TLVE_ERROR;
  }
}

int tlve_check_magic(const char *buf, int *index)
{
  if ((unsigned char)buf[*index] == 222) {
    *index += 1;
    return 0;
  } else {
    return -1;
  }
}

int tlve_get_tuple(const char *buf, int *index)
{return get_recu_element(buf, index);}

int tlve_get_list(const char *buf, int *index)
{return get_recu_element(buf, index);}

int tlve_get_binary(const char *buf, int *index, const char **bin)
{return get_base_element(buf, index, bin);}

int tlve_get_atom(const char *buf, int *index, const char **atom)
{return get_base_element(buf, index, atom);}

int tlve_get_integer(const char *buf, int *index, const char **integer)
{return get_base_element(buf, index, integer);}



//////////////////
// encodings

void tlve_set_magic(char *uf, int *index)
{
  unsigned char *buf = (unsigned char *)uf;
  if (buf) {
    buf[*index] = 222;
  }
  *index += 1;
}

////////////
// helper for list and tuple

static void set_compound_common(char *uf, int len, int *index, char tg)
{
  unsigned char *buf = (unsigned char *)uf;
  unsigned char tag = (unsigned char)tg;
  if (len < 16) {
    if (buf) buf[*index] = tag | len;
    *index += 1;
  } else if (len < 256) {
    if (buf) {
      buf[*index] = tag | 0x11;
      buf[*index+1] = len;
    }
    *index += 2;
  } else if (len < 653356) {
    if (buf) {
      buf[*index] = tag | 0x12;
      buf[*index +1] = len >> 8;
      buf[*index +2] = len & 0xff;
    }
    *index += 3;
  } else if (len < 16777216) {
    if (buf) {
      buf[*index] = tag | 0x13;
      buf[*index +1] = len >> 16;
      buf[*index +2] = (len >> 8) & 0xff;
      buf[*index +3] = len & 0xff;
    }
    *index += 4;
  } else {
    if (buf) {
      buf[*index] = tag | 0x14;
      buf[*index +1] = len >> 24;
      buf[*index +2] = (len >> 16) & 0xff;
      buf[*index +3] = (len >> 8) & 0xff;
      buf[*index +4] = len & 0xff;
    }
    *index += 5;
  }
}

void tlve_set_tuple(char *buf, int len, int *index)
{
  set_compound_common(buf, len, index, 0x00);
}

void tlve_set_list(char *buf, int len, int *index)
{
  set_compound_common(buf, len, index, 0x20);
}

////////////////
// helper for atom, binary and integer

static void set_common(char *uf, int len, char **space, int *index, char tg)
{
  unsigned char *buf = (unsigned char *)uf;
  unsigned char tag = (unsigned char)tg;
  if (len < 16) {
    if (buf) buf[*index] = tag | len;
    if (space) *space = (char *)(buf + *index + 1);
    *index += len + 1;
  } else if (len < 256) {
    if (buf) {
      buf[*index] = tag | 0x11;
      buf[*index+1] = len;
    }
    if (space) *space = (char *)(buf + *index + 2);
    *index += len + 2;
  } else if (len < 653356) {
    if (buf) {
      buf[*index] = tag | 0x12;
      buf[*index +1] = len >> 8;
      buf[*index +2] = len & 0xff;
    }
    if (space) *space = (char *)(buf + *index + 3);
    *index += len + 3;
  } else if (len < 16777216) {
    if (buf) {
      buf[*index] = tag | 0x13;
      buf[*index +1] = len >> 16;
      buf[*index +2] = (len >> 8) & 0xff;
      buf[*index +3] = len & 0xff;
    }
    if (space) *space = (char *)(buf + *index + 4);
    *index += len + 4;
  } else {
    if (buf) {
      buf[*index] = tag | 0x14;
      buf[*index +1] = len >> 24;
      buf[*index +2] = (len >> 16) & 0xff;
      buf[*index +3] = (len >> 8) & 0xff;
      buf[*index +4] = len & 0xff;
    }
    if (space) *space = (char *)(buf + *index + 5);
    *index += len + 5;
  }
}

void tlve_set_binary(char *buf, int len, char **binary, int *index)
{
  set_common(buf, len, binary, index, 0x40);
}

void tlve_set_atom(char *buf, int len, char **atom, int *index)
{
  set_common(buf, len, atom, index, 0x60);
}

void tlve_set_integer(char *buf, int len, char **integer, int *index)
{
  set_common(buf, len, integer, index, 0x80);
}


#ifdef TLVETEST

#include <stdio.h>

//must print 0 3 0 2 0 4
void dencoding_test()
{
  const char atom[] = {222, 98, 111, 107}; //ok
  const char binary[] = {222, 67, 1, 2, 3}; // <<1,2,3>>
  const char integer[] = {222, 129, 42};   // 42
  int index = 0;
  int l = tlve_check_magic(atom, &index);
  printf("%d\n", l);
  printf("%d\n", tlve_get_next_type(atom, index));
  index = 0;
  l = tlve_check_magic(binary, &index);
  printf("%d\n", l);
  printf("%d\n", tlve_get_next_type(binary, index));
  index = 0;
  l = tlve_check_magic(integer, &index);
  printf("%d\n", l);
  printf("%d\n", tlve_get_next_type(integer, index));
}

void encoding_test()
{
  char buf[100];
  int index = 0;
  short *integer;
  tlve_set_integer(buf, 2, (char **)&integer, &index);
  *integer = (short)65535;
  for (int i=0; i<index; ++i) {
    printf("%u\n", (unsigned char)buf[i]);
  }
}


int main()
{

  encoding_test();

  return 0;
}

#endif
