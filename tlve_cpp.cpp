#include "tlve_cpp.hpp"

#include "host_to_net.hpp"
#include "Recoil.hpp"

#include <cstring>

#include <string>
#include <sstream>
#include <functional>


///////////////////
// encoder

tlve_encoder::tlve_encoder(size_t s)
    : buffer_ref_(new char [s]),
    buffer_(&(buffer_ref_[0])),
    index_(0)
{
  tlve_set_magic(buffer_, &index_);
}

tlve_encoder::tlve_encoder(char *b)
    : buffer_(b),
    index_(0)
{
  tlve_set_magic(buffer_, &index_);
}

char *tlve_encoder::get_data()
{
  return buffer_;
}

int tlve_encoder::get_data_len()
{
  return index_;
}

tlve_encoder &tlve_encoder::tuple(int len)
{
  tlve_set_tuple(buffer_, len, &index_);
  return *this;
}

tlve_encoder &tlve_encoder::list(int len)
{
  tlve_set_list(buffer_, len, &index_);
  return *this;
}

tlve_encoder &tlve_encoder::binary(int len, char **binary)
{
  tlve_set_binary(buffer_, len, binary, &index_);
  return *this;
}

tlve_encoder &tlve_encoder::atom(int len, char **atom)
{
  tlve_set_atom(buffer_, len, atom, &index_);
  return *this;
}

tlve_encoder &tlve_encoder::atom(const char *a)
{
  int len = strlen(a);
  char *atom;
  tlve_set_atom(buffer_, len, &atom, &index_);
  memcpy(atom, a, len);
  return *this;
}


tlve_encoder &tlve_encoder::integer(int len, char **integer)
{
  tlve_set_integer(buffer_, len, integer, &index_);
  return *this;
}

tlve_encoder &tlve_encoder::integer(int i)
{
  auto f = [](int i) -> int {
    if (-129 < i && i < 128) return 1;
    if (-32769 < i && i < 32768) return 2;
    if (-16777217 < i && i < 16777216) return 3;
    return 4;
  };
  char *s_i;
  int len = f(i);
  tlve_set_integer(buffer_, len, &s_i, &index_);
  switch (f(i)) {
    case 1:
      *s_i = i;
      break;
    case 2:
      host_to_net <2, BIG_ENDIAN1> ((char *)&i, s_i);
      break;
    case 3:
      host_to_net <3, BIG_ENDIAN1> ((char *)&i, s_i);
      break;
    case 4:
      host_to_net <4, BIG_ENDIAN1> ((char *)&i, s_i);
      break;
  }
  return *this;
}


tlve_encoder &tlve_encoder::prepare_rpc_call
    (const char *in_mod,
    const char *in_fun)
{
  char *cmd;
  char *mod;
  char *fun;
  this->tuple(4).
    atom(1, &cmd).
    atom(strlen(in_mod), &mod).
    atom(strlen(in_fun), &fun);
  *cmd = 'c';
  strncpy(mod, in_mod, strlen(in_mod));
  strncpy(fun, in_fun, strlen(in_fun));
  return *this;
}

tlve_encoder &tlve_encoder::prepare_rpc_call(const char *in_fun)
{
  char *cmd;
  char *fun;
  this->tuple(3).
    atom(1, &cmd).
    atom(strlen(in_fun), &fun);
  *cmd = 'c';
  strncpy(fun, in_fun, strlen(in_fun));
  return *this;
}


tlve_encoder &tlve_encoder::prepare_rpc_event
    (const char *in_mod,
    const char *in_fun)
{
  char *cmd;
  char *mod;
  char *fun;
  this->tuple(4).
    atom(1, &cmd).
    atom(strlen(in_mod), &mod).
    atom(strlen(in_fun), &fun);
  *cmd = 'e';
  strncpy(mod, in_mod, strlen(in_mod));
  strncpy(fun, in_fun, strlen(in_fun));
  return *this;
}

tlve_encoder &tlve_encoder::prepare_rpc_event(const char *in_fun)
{
  char *cmd;
  char *fun;
  this->tuple(3).
    atom(1, &cmd).
    atom(strlen(in_fun), &fun);
  *cmd = 'e';
  strncpy(fun, in_fun, strlen(in_fun));
  return *this;
}

tlve_encoder &tlve_encoder::prepare_rpc_reply()
{
  char *cmd;
  this->tuple(2).
    atom(1, &cmd);
  *cmd = 'r';
  return *this;
}

tlve_encoder &tlve_encoder::prepare_rpc_error()
{
  char *cmd;
  this->tuple(2).
    atom(1, &cmd);
  *cmd = 'e';
  return *this;
}


/////////////
// decoder


tlve_decoder::tlve_decoder(const char *buf)
    : buffer_(buf),
    index_(0)
{
  if (tlve_check_magic(buffer_, &index_) != 0) {
    throw -1;
  }
}


enum tlve_decoder::type tlve_decoder::next_type()
{
  switch (tlve_get_next_type(buffer_, index_)) {
    case ::TLVE_TUPLE:
      return TLVE_TUPLE;
    case ::TLVE_LIST:
      return TLVE_LIST;
    case ::TLVE_BINARY:
      return TLVE_BINARY;
    case ::TLVE_ATOM:
      return TLVE_ATOM;
    case ::TLVE_INTEGER:
      return TLVE_INTEGER;
    default:
      break;
  }
  return TLVE_ERROR;
}

int tlve_decoder::tuple()
{
  int ret;
  if ((ret = tlve_get_tuple(buffer_, &index_)) < 0) throw -1;
  return ret;
}

int tlve_decoder::list()
{
  int ret;
  if ((ret = tlve_get_list(buffer_, &index_)) < 0) throw -1;
  return ret;
}

int tlve_decoder::binary(const char **ret)
{
  int rt;
  if ((rt = tlve_get_binary(buffer_, &index_, ret)) < 0) throw -1;
  return rt;
}

int tlve_decoder::atom(const char **ret)
{
  int rt;
  if ((rt = tlve_get_atom(buffer_, &index_, ret)) < 0) throw -1;
  return rt;
}

int tlve_decoder::integer(const char **ret)
{
  int rt;
  if ((rt = tlve_get_integer(buffer_, &index_, ret)) < 0) throw -1;
  return rt;
}

int tlve_decoder::integer()
{
  //TODO: be platform
  const unsigned char *dd;
  int ret;
  switch (integer((const char **)&dd)) {
    case 1:
      ret = *dd; ret <<= 24; ret >>= 24;
      break;
    case 2:
      ret = dd[0] * 256 + dd[1]; ret <<= 16; ret >>= 16;
      break;
    case 3:
      ret = dd[0] * 65536 + dd[1] * 256 + dd[2]; ret <<= 8; ret >>= 8;
      break;
    case 4:
      ret = dd[0] * 16777216 + dd[1] * 65536 + dd[2] * 256 + dd[3];
      break;
    default:
      throw -1;
  }
  return ret;
}



bool tlve_decoder::is_rpc(const char *in_mod, const char *in_fun, char in_cmd)
{
  int last_index = index_;
  Recoil <std::function <void ()> > recoil([&] () {index_ = last_index;});
  const char *cmd;
  const char *mod; int mod_len;
  const char *fun; int fun_len;
  if (tuple() == 4) {
    if (next_type() == TLVE_ATOM) {
      atom(&cmd);
      if (next_type() == TLVE_ATOM) {
        mod_len = atom(&mod);
        if (next_type() == TLVE_ATOM) {
          fun_len = atom(&fun);
          size_t in_mod_len = strlen(in_mod);
          size_t in_fun_len = strlen(in_fun);
          if ((*cmd == in_cmd) &&
              !strncmp(in_mod, mod,
                (int)in_mod_len > mod_len? mod_len: in_mod_len) &&
              !strncmp(in_fun, fun,
                (int)in_fun_len > fun_len? fun_len: in_fun_len)) {
                  recoil.discharge();
                  return true;
          }
        }
      }
    }
  }
  return false;
}

bool tlve_decoder::is_rpc(const char *in_fun, char in_cmd)
{
  int last_index = index_;
  Recoil <std::function <void ()> > recoil([&] () {index_ = last_index;});
  const char *cmd;
  const char *fun; int fun_len;
  if (tuple() == 3) {
    if (next_type() == TLVE_ATOM) {
      atom(&cmd);
      if (next_type() == TLVE_ATOM) {
        fun_len = atom(&fun);
        size_t in_fun_len = strlen(in_fun);
        if ((*cmd == in_cmd) &&
            !strncmp(in_fun, fun,
              (int)in_fun_len > fun_len? fun_len: in_fun_len)) {
                recoil.discharge();
                return true;
        }
      }
    }
  }
  return false;
}

bool tlve_decoder::is_rpc_call(const char *in_mod, const char *in_fun)
{
  return is_rpc(in_mod, in_fun, 'c');
}

bool tlve_decoder::is_rpc_call(const char *in_fun)
{
  return is_rpc(in_fun, 'c');
}

bool tlve_decoder::is_rpc_event(const char *in_mod, const char *in_fun)
{
  return is_rpc(in_mod, in_fun, 'e');
}

bool tlve_decoder::is_rpc_event(const char *in_fun)
{
  return is_rpc(in_fun, 'e');
}

bool tlve_decoder::is_rpc_back(char cmd)
{
  int last_index = index_;
  Recoil <std::function <void ()> > recoil([&] () {index_ = last_index;});
  const char *atm;
  if (tuple() == 2) {
    if (next_type() == TLVE_ATOM) {
      atom(&atm);
      if (*atm == cmd) {
        recoil.discharge();
        return true;
      }
    }
  }
  return false;
}

bool tlve_decoder::is_rpc_reply()
{
  return is_rpc_back('r');
}

bool tlve_decoder::is_rpc_error()
{
  return is_rpc_back('e');
}

#ifdef TLVE_C_TEST

int tlve_to_string_atom_tuple()
{
  tlve_encoder t(100);
  char *atom1;
  char *atom2;
  t.tuple(2).atom(2, &atom1).atom(3, &atom2);
  atom1[0] = 'o'; atom1[1] = 'k';
  atom2[0] = 'a'; atom2[1] = 'b'; atom2[2] = 'c';
  printf("%s\n", tlve_to_string(t.get_data()).c_str());
}

int tlve_to_string_binary()
{
  tlve_encoder t(100);
  char *b;
  char *atom;
  char *integer;
  t.tuple(3).binary(3, &b).atom(1,&atom).integer(42);
  b[0] = 1; b[1] = 2; b[2] = 130;
  atom[0] = 'a';
  printf("%s\n", tlve_to_string(t.get_data()).c_str());
}


int encoder_test()
{
  tlve_encoder t(100);
  t.tuple(3);
  char *a;
  t.atom(2, &a);
  a[0] = 'o';
  a[1] = 'k';
  t.binary(3, &a);
  a[0] = 1;
  a[1] = 2;
  a[3] = 3;
  t.integer(2, &a);
  a[0] = 1;
  a[1] = 0;

  int len;
  char *buffer = t.get_data();
  //for (int i=0; i<t; ++i) {
    write(1, buffer, len);
  //}
}

int main()
{
  tlve_to_string_binary();
}


#endif
