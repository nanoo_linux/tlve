#pragma once

#include <cstdint>

template <int, bool>
void host_to_net (char *, char *);


#ifdef __linux__
  #if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    #define BIG_ENDIAN1 false
  #else
    #define BIG_ENDIAN1 true
  #endif
#else
  #define BIG_ENDIAN1 false
#endif
