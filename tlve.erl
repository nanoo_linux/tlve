-module(tlve). % type-len-value encoding

-vsn("0.0.1").

-export([from_term/1, to_term/1]).


to_term(<<16#de, Rest/binary>>) ->
  Fun = fun(_, A) -> A end,
  {<<>>, Ret} = to_term({Rest, []}, Fun),
  Ret.

to_term({Data, Ret}, Fun) ->
  <<TL, Rest/binary>> = Data,
  <<Code:3, _:5>> = <<TL>>,
  CopyFn = fun(_, X) -> X end,
  case Code of
    0 ->
      F2 = fun(X, Y) -> erlang:append_element(X, Y) end,
      {RestBinary, {Tuple}} = to_tuple(TL, {Rest, {}}, F2),
      {RestBinary, Fun(Ret, Tuple)};
    1 ->
      F2 = fun(X, Y) -> lists:append(X, [Y]) end,
      {RestBinary, [List]} = to_list(TL, {Rest, []}, F2),
      {RestBinary, Fun(Ret, List)};
    2 ->
      {RestBinary, Bin} = to_binary(TL, {Rest, []}, CopyFn),
      {RestBinary, Fun(Ret, Bin)};
    3 ->
      {RestBinary, Atom} = to_atom(TL, {Rest, []}, CopyFn),
      {RestBinary, Fun(Ret, Atom)};
    4 ->
      {RestBinary, Int1} = to_int(TL, {Rest, []}, CopyFn),
      {RestBinary, Fun(Ret, Int1)};
    _ ->
      erlang:error(wrong_data)
  end.

to_tuple(TL, {Data, Ret}, Fun) ->
  case <<TL>> of
    <<0:3, 0:1, L:4>> ->
      {Rest, Tuple} = to_tuple(L, {Data,{}}),
      {Rest, Fun(Ret, Tuple)};
    <<0:3, 1:1, L:4>> ->
      L2 = L *8,
      <<Tl:L2/unsigned, R1/binary>> = Data,
      {Rest, Tuple} = to_tuple(Tl, {R1,{}}),
      {Rest, Fun(Ret, Tuple)}
  end.

to_tuple(0, {Data, Ret}) -> {Data,Ret};
to_tuple(Len, {Data, Ret}) ->
  F = fun(E, X) -> erlang:append_element(E, X) end,
  {RetData, Tuple} = to_term({Data, Ret}, F),
  to_tuple(Len-1, {RetData, Tuple}).

to_list(16#20, {Data, Ret}, Fun) -> {Data, Fun(Ret, [])};
to_list(TL, {Data, Ret}, Fun) ->
  case <<TL>> of
    <<1:3, 0:1, L:4>> ->
      {Rest, List} = to_list(L, {Data, []}),
      {Rest, Fun(Ret, List)};
    <<1:3, 1:1, L:4>> ->
      L2 = L*8,
      <<Len:L2/signed, Rest/binary>> = Data,
      {Rest2, List} = to_list(Len, {Rest, []}),
      {Rest2, Fun(Ret, List)}
  end.

to_list(0, {Data, L}) ->
  F = fun(X, Y) -> lists:append(X, Y) end,
  <<TL, Rest/binary>> = Data,
  to_list(TL, {Rest, L}, F);
to_list(N, {Data, L}) ->
  F = fun(E, X) -> lists:append(E, [X]) end,
  {RetData, List} = to_term({Data, L}, F),
  to_list(N-1, {RetData, List}).

to_binary(TL, {Data, Ret}, Fun) ->
  case <<TL>> of
    <<2:3, 0:1, L:4>> ->
      <<A:L/binary, Rest/binary>> = Data,
      {Rest, Fun(Ret, A)};
    <<2:3, 1:1, L:4>> ->
      L2 = L*8,
      <<Al:L2/unsigned, Rest/binary>> = Data,
      <<A:Al/binary, Rest2/binary>> = Rest,
      {Rest2, Fun(Ret, A)}
  end.

to_atom(TL, {Data, Ret}, Fun) ->
  case <<TL>> of
    <<3:3, 0:1, L:4>> ->
      <<A:L/binary, Rest/binary>> = Data,
      {Rest, Fun(Ret, list_to_atom(binary_to_list(A)))};
    <<3:3, 1:1, L:4>> ->
      L2 = L*8,
      <<Bl:L2/unsigned, Rest/binary>> = Data,
      <<A:Bl/binary, Rest2/binary>> = Rest,
      {Rest2, Fun(Ret, list_to_atom(binary_to_list(A)))}
  end.

to_int(TL, {Data, Ret}, Fun) ->
  <<4:3, 0:1, L:4>> = <<TL>>,
  L2 = L*8,
  <<I:L2/signed, Rest/binary>> = Data,
  {Rest, Fun(Ret, I)}.


from_term(T) ->
  from_term(T, <<16#de>>).

from_term(T, R) when is_binary(T) ->
  S = size(T),
  if
    S < 16 ->
      <<TL>> = <<2:3, 0:1, S:4>>,
      <<R/binary, TL, T/binary>>;
    S < 256 ->
      TL = <<2:3, 1:1, 1:4, S:8/unsigned>>,
      <<R/binary, TL/binary, T/binary>>;
    S < 65535 ->
      TL = <<2:3, 1:1, 2:4, S:16/unsigned>>,
      <<R/binary, TL/binary, T/binary>>;
    S < 16777216 ->
      TL = <<2:3, 1:1, 3:4, S:24/unsigned>>,
      <<R/binary, TL/binary, T/binary>>;
    true ->
      TL = <<2:3, 1:1, 4:4, S:32/unsigned>>,
      <<R/binary, TL/binary, T/binary>>
  end;

from_term(T, R) when is_atom(T) ->
  A = atom_to_list(T),
  BA = list_to_binary(A),
  S = length(A),
  if
    S < 16 ->
      <<TL>> = <<3:3, 0:1, S:4>>,
      <<R/binary, TL, BA/binary>>;
    S < 256 ->
      TL = <<3:3, 1:1, 1:4, S:8/unsigned>>,
      <<R/binary, TL/binary, BA/binary>>;
    S < 65536 ->
      TL = <<3:3, 1:1, 2:4, S:16/unsigned>>,
      <<R/binary, TL/binary, BA/binary>>;
    S < 16777216 ->
      TL = <<3:3, 1:1, 3:4, S:24/unsigned>>,
      <<R/binary, TL/binary, BA/binary>>;
    true ->
      TL = <<3:3, 1:1, 4:4, S:32/unsigned>>,
      <<R/binary, TL/binary, BA/binary>>
  end;

from_term(T, R) when is_integer(T) ->
  F = fun(X, S) -> <<Y:S/signed>> = <<X:S>>, Y == X end,
  case {F(T, 8), F(T, 16), F(T, 24), F(T, 32)} of
    {true, true, true, true} ->
      <<TL>> = <<4:3, 0:1, 1:4>>,
      <<R/binary, TL, T:8>>;
    {false, true, true, true} ->
      <<TL>> = <<4:3, 0:1, 2:4>>,
      <<R/binary, TL, T:16>>;
    {false, false, true, true} ->
      <<TL>> = <<4:3, 0:1, 3:4>>,
      <<R/binary, TL, T:24>>;
    {false, false, false, true} ->
      <<TL>> = <<4:3, 0:1, 4:4>>,
      <<R/binary, TL, T:32>>;
    {false, false, false, false} ->
      <<TL>> = <<4:3, 0:1, 8:4>>,
      <<R/binary, TL, T:64>>
  end;

from_term(T, R) when is_tuple(T) ->
  S = size(T),
  if
    S < 16 ->
      <<TL>> = <<0:3, 0:1, S:4>>,
      R2 = from_tuple(0, S, T, <<>>),
      <<R/binary, TL, R2/binary>>;
    S < 256 ->
      <<TL>> = <<0:3, 1:1, 1:4>>,
      R2 = from_tuple(0, S, T, <<>>),
      <<R/binary, TL, S, R2/binary>>;
    S < 65536 ->
      <<TL>> = <<0:3, 1:1, 2:4>>,
      R2 = from_tuple(0, S, T, <<>>),
      <<R/binary, TL, S:16, R2/binary>>;
    S < 16777216 ->
      <<TL>> = <<0:3, 1:1, 3:4>>,
      R2 = from_tuple(0, S, T, <<>>),
      <<R/binary, TL, S:24, R2/binary>>;
    true ->
      <<TL>> = <<0:3, 1:1, 4:4>>,
      R2 = from_tuple(0, S, T, <<>>),
      <<R/binary, TL, S:32, R2/binary>>
  end;

from_term(T, R) when is_list(T) ->
  S = length(T),
  if
    S == 0 ->
      <<R/binary, 16#20>>;
    S < 16 ->
      <<TL>> = <<1:3, 0:1, S:4>>,
      R2 = from_list(T, <<>>),
      <<R/binary, TL, R2/binary>>;
    S < 256 ->
      <<TL>> = <<1:3, 1:1, 1:4>>,
      R2 = from_list(T, <<>>),
      <<R/binary, TL, S, R2/binary>>;
    S < 65536 ->
      <<TL>> = <<1:3, 1:1, 2:4>>,
      R2 = from_list(T, <<>>),
      <<R/binary, TL, S:16, R2/binary>>;
    S < 16777216 ->
      <<TL>> = <<1:3, 1:1, 3:4>>,
      R2 = from_list(T, <<>>),
      <<R/binary, TL, S:24, R2/binary>>;
    true ->
      <<TL>> = <<1:3, 1:1, 4:4>>,
      R2 = from_list(T, <<>>),
      <<R/binary, TL, S:32, R2/binary>>
  end.

from_tuple(L, L, _, B) -> B;
from_tuple(N, L, T, B) ->
  B1 = from_term(element(N+1, T), B),
  from_tuple(N+1, L, T, B1).

from_list([], B) -> <<B/binary, 32>>;
from_list([H|T], B) ->
  B2 = from_term(H, B),
  from_list(T, B2).
