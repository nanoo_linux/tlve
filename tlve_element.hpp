#pragma once

#include <string>
#include <memory>
#include <vector>
#include <functional>
#include "tlve_cpp.hpp"


class tlve_element;

typedef std::shared_ptr <tlve_element> p_tlve_element;


class tlve_element
{
  public:
    enum type {TLVE_TUPLE, TLVE_LIST, TLVE_ATOM, TLVE_INTEGER, TLVE_BINARY};
    enum type type() const {return type_;}
    tlve_element(enum type t)
      : type_(t) {}
    virtual ~tlve_element() {}

    virtual std::vector <p_tlve_element> &elements() = 0;
    virtual int &size() = 0;
    virtual const std::string &atom() = 0;
    virtual long long value() = 0;
    virtual const char *data() = 0;

  private:
    enum type type_;
};



class tlve_tuple: public tlve_element
{
  std::vector <p_tlve_element> elems_;
  int size_;
  public:
    tlve_tuple() : tlve_element(TLVE_TUPLE) {}
    std::vector <p_tlve_element> &elements() {return elems_;}
    int &size() {return size_;}

    const std::string &atom() {throw -1;}
    long long value() {throw -1;}
    const char *data() {throw -1;}
};

class tlve_list: public tlve_element
{
  std::vector <p_tlve_element> elems_;
  int size_;
  public:
    tlve_list() : tlve_element(TLVE_LIST) {}
    std::vector <p_tlve_element> &elements() {return elems_;}
    int &size() {return size_;}

    const std::string &atom() {throw -1;}
    long long value() {throw -1;}
    const char *data() {throw -1;}
};

class tlve_atom: public tlve_element
{
  std::string atom_;
  public:
    tlve_atom(std::string &n) : tlve_element(TLVE_ATOM), atom_(n) {}
    std::string &atom() {return atom_;}

    std::vector <p_tlve_element> &elements() {throw -1;}
    int &size() {throw -1;}
    long long value() {throw -1;}
    const char *data() {throw -1;}
};

class tlve_integer: public tlve_element
{
  long long value_;
  public:
    tlve_integer(long long n) : tlve_element(TLVE_INTEGER), value_(n) {}
    long long value() {return value_;}

    std::vector <p_tlve_element> &elements() {throw -1;}
    int &size() {throw -1;}
    const std::string &atom() {throw -1;}
    const char *data() {throw -1;}
};

class tlve_binary: public tlve_element
{
  const char *data_;
  int size_;
  public:
    tlve_binary(const char *d, int s)
      : tlve_element(TLVE_BINARY), data_(d), size_(s) {}
    const char *data() {return data_;}
    int &size() {return size_;}

    std::vector <p_tlve_element> &elements() {throw -1;}
    long long value() {throw -1;}
    const std::string &atom() {throw -1;}
};


p_tlve_element tlve_to_element(const char *);
