  Definitions.

tlve: 0xde, element

element: tuple | list | atom | integer | binary

elements: element | element, elements

tuple: {} | {elements}

list : [] | [elements]

atom : "any set of ascii chars"

integer: signed integer any length

byte : 0-255

bytes : byte | byte, bytes

binary: <<>> | <<bytes>>



  Rpc.

call:
  > {c, Module, Fun, [Args]}
  < {r, [Ret]} | {e, [Reason]}

  > {c, Fun, [Args]}
  < {r, [Ret]} | [e, [Reason]}

events:
  > {e, Module, Fun, [Args]}
  > {e, Fun, [Args]}




  Encoding.

tag, len0, len1
000, 0, 0000 [more bytes]

tag:
  0 - tuple,
  1 - list
  2 - binary
  3 - atom
  4 - integer
len0 -
  0 - len1 is number of elements follow in tuple and list; number of bytes
    in atom, binary, integer.
  1 - len1 is number of bytes follow which are Length in network byte order.
    Length is number of elements follow in tuple and list; number of bytes
    of atom, binary, integer.
len1 -
  see len0.

ex:
0x00 (000 0 0000) - {}
0x20 (001 0 0000) - []
0x01, 0x00 - {{}}
0x02, 0x00, 0x00  - {{}, {}}
0x02, 0x20, 0x20  - {[], []}
0x41, 0x01 (010 0 0001) - <<1>>
0x42, 0x01, 0x03 - <<1, 3>>
0x62, $o, $k, - ok
0x65, $e,$r,$r,$o,$r - error
0x81, 0xff (110 0 0001) - -1
0x82, 0x00, 0xff - 255




 Lists.

True length (except 0) is always one less, then expected. Last element must
be empty list(finish), or nonempty list(with more data).

ex:
[] - [] (0x20)
[1, 2, 3] - list of 4 (!) elements: 1, 2, 3 and []. but as length 3 encoded.
[1, 2, 3] - list of 3 (!) elements: 1, 2, and [3]. but as length 2 encoded.
[3] - list of 2 (!) elements: 3, and []. but as length 1 encoded.

[{},{}] - 0x22, 0x00, 0x00, 0x20
[{},{}] - 0x21, 0x00, 0x21, 0x00, 0x20
